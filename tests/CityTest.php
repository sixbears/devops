<?php

require_once __DIR__ . '/../src/vendor/autoload.php'; // Autoload files using Composer autoload
include_once(__DIR__ . "/../src/City.php");

class CityTest extends \PHPUnit\Framework\TestCase
{

    public function testgetName()
    {
        $city = new City("Bordeaux", "France");
        $result = $city->getName();
        $expected = 'Bordeaux';
        $this->assertTrue($result == $expected);
    }

    public function testgetCountry()
    {
        $city = new City("Bordeaux", "France");
        $result = $city->getCountry();
        $this->expectOutputString($result);
        print 'France';
    }
}
