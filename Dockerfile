FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -yq --no-install-recommends \   
    # Install apache
    apache2 \
    # Install php 7.2
    libapache2-mod-php7.4 \
    # Install tools
    locales \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# Set locales
RUN locale-gen fr_FR.UTF-8

COPY php.ini /etc/php/7.2/mods-available/
RUN phpenmod conf
# Configure apache for conf
RUN a2enmod rewrite expires
RUN echo "ServerName localhost" | tee /etc/apache2/conf-available/servername.conf
RUN a2enconf servername
# Configure vhost for conf
COPY conf.conf /etc/apache2/sites-available/
RUN a2dissite 000-default
RUN a2ensite conf.conf

# Heure et date
RUN echo "Europe/Paris" > /etc/timezone
RUN cp /usr/share/zoneinfo/Europe/Paris /etc/localtime
RUN dpkg-reconfigure -f noninteractive tzdata
# Suppression du répertoire html
RUN rm -rf /var/www/html 

EXPOSE 80 443

WORKDIR /var/www/

ADD src /var/www/ 
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
